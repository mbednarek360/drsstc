import socket, sys

ser = open("/dev/snd/midiC2D0", "rb", buffering=0)

tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('10.0.0.1', 21212)
tcp_socket.connect(server_address)

while True:
    tcp_socket.send(ser.read(1))
