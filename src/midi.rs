use super::pwm;
use std::io::Read;
use std::net::TcpListener;
use std::sync::{Arc, Mutex};

// one byte per channel
// value from 0 - 127

pub fn init_loop(iter: Arc<Mutex<pwm::PulseIterator>>) {
    // let mut f = File::open("/dev/snd/midiC2D0").unwrap();
    let listener = TcpListener::bind("0.0.0.0:21212").unwrap();

    for stream in listener.incoming() {
        let mut f = stream.unwrap();

        let (mut new, mut status) = (vec![0u8], vec![0u8]); // status byte buffers
        let mut note = vec![0u8; 2]; // note bytes buffer
        let mut pitch = 0u128;

        // will update the shared iterator when necessary
        loop {
            f.read_exact(&mut new).unwrap();
            status[0] ^= (status[0] ^ new[0]) * (new[0] >> 7); // update status if new status

            match status[0] >> 4 {

                // note on / off
                0x8 | 0x9 => {

                    // read remaining note byte(s)
                    if new[0] & (1 << 7) == 0 {
                        note[0] = new[0];
                        f.read_exact(&mut new).unwrap();
                        note[1] = new[0];
                    } else {
                        f.read_exact(&mut note).unwrap();
                    }
                    // set the bit low for that pitch
                    pitch &= !(1 << note[0]);

                    // set the bit high if note on
                    let on = (status[0] >> 4) == 0x9 && (note[1] != 0);
                    pitch |= (1 << note[0]) * on as u128;

                    // update
                    iter.lock().unwrap().update(pitch);
                }
                _ => (),
            }
        }
    }
}
