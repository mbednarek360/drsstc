pub mod midi;
pub mod pwm;
use std::sync::{Arc, Mutex};

fn main() {
    let iter = Arc::new(Mutex::new(pwm::PulseIterator::new()));

    // pwm on new thread
    pwm::init_loop(iter.clone());

    // midi on main thread
    midi::init_loop(iter);
}
