use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;
use rppal::gpio::{Gpio, OutputPin};
use shuteye::sleep;

const ON_TIME: u64 = 10; // ontime in us
const OFF_CYCLES: u64 = 9; // offtime / ontime
const GPIO_PIN: u8 = 26; // gpio 26

// can play from C2 - B4 (36 - 71)
// gives 1/f in us starting at C2 
pub const PERIOD: [u64; 36] = [15289, 14431, 13621, 12856, 12135, 11454, 10811, 10204, 9631, 9091, 8581, 8099, 7645, 7215, 6810, 6428, 6067, 5727, 5405, 5102, 4816, 4545, 4290, 4050, 3822, 3608, 3405, 3214, 3034, 2863, 2703, 2551, 2408, 2273, 2145, 2025];

pub struct PulseIterator {
    notes: u64,
    cycles: [u64; PERIOD.len()],
}

// returns offtime till next spark
// we want to find the lagging frequency and get the difference to the next
impl PulseIterator {
    pub fn next(&mut self) -> u64 {
       
        // init min phase and index
        let (mut min, mut index) = (u64::MAX, 0usize);

        // iterate through notes 0 - 35
        for i in 0..PERIOD.len() {

            // check if current note is enabled, delete its cycle if not
            let on = (self.notes >> i) & 0x1; 
            self.cycles[i] *= on; 
            
            // calculate phase of current note
            let phase = self.cycles[i] * PERIOD[i];

            // update min and index if its the lowest phase
            if phase < min && on == 1 { 
                min = phase; 
                index = i;
            } 
        }
        if min == u64::MAX {
            return min;
        }

        // iterate cycle of lowest phase note if enabled
        self.cycles[index] += (self.notes >> index) & 0x1;
        
        // calculate distance between next and previous phase
        let delay = self.cycles[index] * PERIOD[index] - min - ON_TIME;
        if delay < OFF_CYCLES * ON_TIME {
            return self.next();
        }
        else {
            return delay;
        }
    }

    #[inline(always)]
    pub fn update(&mut self, pitch: u128) {
        self.notes = (pitch >> 36) as u64;
        self.cycles = [0u64; PERIOD.len()];
    }

    pub fn new() -> Self {
        Self {
            notes: 0u64,
            cycles: [0u64; PERIOD.len()],
        }
    }
}

pub fn init_loop(iter: Arc<Mutex<PulseIterator>>) {
    let gpio = Gpio::new().unwrap();
    let mut pin = gpio.get(GPIO_PIN).unwrap().into_output();
    thread::spawn(move || loop {
        let off = iter.lock().unwrap().next();
        if off == u64::MAX {
            continue;
        }
        println!("{}", off);
        spark(&mut pin); 
        thread::sleep(Duration::from_micros(off));
        // thread::sleep(Duration::from_micros(100));
    });
}

fn spark(pin: &mut OutputPin) {
    pin.set_high();
    sleep(Duration::new(0, ON_TIME as u32 * 1000));
    pin.set_low();
}
