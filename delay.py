import math

def delay(m):
    return 1e6 / (math.pow(2, (m-69) / 12) * 440)

d = [round(delay(m)) for m in range(36, 72)]
print(d)
